import { Component, OnInit } from "@angular/core";
import { MusicService } from "../music.service";
import { tap, catchError } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
  // viewProviders: [MusicService]
})
export class MusicSearchComponent implements OnInit {
  albums$ = this.musicService
    .getAlbums()
    .pipe(catchError(err => (this.message = err.message)));

  query$ = this.musicService.getQuery().pipe(
    tap(query => {
      this.router.navigate([], {
        queryParams: { query }
      });
    })
  );

  message: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private musicService: MusicService
  ) {}

  search(query) {
    this.musicService.searchAlbums(query);
  }

  ngOnInit() {
    const query = this.route.snapshot.queryParamMap.get("query");

    if (query !== null) {
      this.search(query);
    }
  }
}
