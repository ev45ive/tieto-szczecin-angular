import { Component, OnInit, HostBinding, Input } from "@angular/core";
import { Album, AlbumImage } from "../../models/album";

@Component({
  selector: "app-album-item, [app-album-item]",
  templateUrl: "./album-item.component.html",
  styleUrls: ["./album-item.component.scss"]
})
export class AlbumItemComponent implements OnInit {
  album: Album;

  image: AlbumImage;

  @Input("album")
  set albumsetter(album) {
    this.album = album;
    this.image = album.images[0] || {
      url: "default.png"
    };
  }

  // @HostBinding('class.card') classCard = true

  constructor() {}

  ngOnInit() {}
}
