import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchComponent } from "./music-search/music-search.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumGridComponent } from "./album-grid/album-grid.component";
import { AlbumItemComponent } from "./album-item/album-item.component";
import { environment } from "src/environments/environment";
import { MusicService } from "./music.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    MusicRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumGridComponent,
    AlbumItemComponent
  ],
  // exports: [MusicSearchComponent], // because Routing
  /* === */
  providers: [
    {
      provide: "MUSIC_API_URL",
      useValue: environment.music_search.api_url
    }
    // {
    //   provide: "MusicService",
    //   useFactory: api_url => {
    //     return new MusicService(api_url);
    //   },
    //   deps: ["MUSIC_API_URL"]
    // }
    // {
    //   provide: "MusicService",
    //   useClass: MusicService,
    //   // deps: ["MUSIC_API_URL"]
    // },
    // {
    //   provide: AbstractMusicService,
    //   useClass: SpecialChristmassMusicService
    // },
    // MusicService
  ]
})
export class MusicModule {}
