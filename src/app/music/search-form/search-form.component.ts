import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { map, tap, withLatestFrom } from "rxjs/operators";
import {
  FormGroup,
  AbstractControl,
  FormControl,
  FormArray,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import { filter, distinctUntilChanged, debounceTime } from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;
  @Input()
  set query(q) {
    this.queryForm.get("query").setValue(q, {
      emitEvent: false
    });
  }

  constructor() {
    const censor = (badword: string): ValidatorFn => (
      control: FormControl
    ): ValidationErrors | null => {
      const hasError = (control.value as string).includes(badword);

      return hasError
        ? {
            censor: { badword: badword }
          }
        : null;
    };

    const asyncCensor = (badword: string): AsyncValidatorFn => (
      control: FormControl
    ) => {
      // return this.http.get(/validatate,{params:{q:control.value}}).pipe(map(response=>response.errors))

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          const handle = setTimeout(() => {
            const hasError = (control.value as string).includes(badword);
            observer.next(
              hasError
                ? {
                    censor: { badword: badword }
                  }
                : null
            );
            observer.complete();
          }, 1000);

          return () => {
            clearTimeout(handle);
          };
        }
      );
    };

    this.queryForm = new FormGroup({
      query: new FormControl("", {
        validators: [Validators.required, Validators.minLength(3)],
        asyncValidators: [asyncCensor("batman")]
      })
    });

    const status$ = this.queryForm.get("query").statusChanges;

    const valid$ = status$.pipe(filter(status => status == "VALID"));

    const value$ = this.queryForm.get("query").valueChanges;

    this.queryChange = valid$.pipe(
      withLatestFrom(value$, (_, value) => value),
      map(query => query.trim()),
      distinctUntilChanged()
    );
  }

  @Output() queryChange;

  ngOnInit() {}
}
