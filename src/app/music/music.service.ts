import { Injectable, Inject } from "@angular/core";
import { Album } from "src/app/models/album";
import { HttpClient } from "@angular/common/http";

import { map, startWith, switchMap } from "rxjs/operators";
import { of, Subject, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MusicService {
  query$ = new BehaviorSubject<string>("batman");
  albums$ = new BehaviorSubject<Album[]>([]);

  constructor(
    @Inject("MUSIC_API_URL") private api_url: string,
    private http: HttpClient
  ) {
    this.query$
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.api_url, { params })
        ),
        map(response => response.albums.items)
      )
      .subscribe(albums => {
        this.albums$.next(albums);
      });
  }

  searchAlbums(query = "batman") {
    this.query$.next(query);
  }

  getAlbums() {
    return this.albums$.asObservable();
  }

  getQuery() {
    return this.query$.asObservable();
  }
}

interface AlbumsResponse {
  albums: {
    items: Album[];
  };
}
