import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    AuthInterceptorService,
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: AuthInterceptorService,
      multi: true
    }
  ]
})
export class SecurityModule {}
