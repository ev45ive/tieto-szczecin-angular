import { Injectable, InjectionToken, Inject } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export const SECURITY_CONFIG = new InjectionToken<SecurityConfig>(
  "Security Service Config"
);

@Injectable({
  providedIn: "root"
})
export class SecurityService {
  constructor(@Inject(SECURITY_CONFIG) private config: SecurityConfig) {}

  authorize() {
    // Destructurize config
    const { client_id, response_type, redirect_uri } = this.config;

    // Build params string
    const params = new HttpParams({
      fromObject: {
        client_id,
        response_type,
        redirect_uri
      }
    });

    // Build url
    const redirectURL = this.config.auth_url + "?" + params.toString();

    localStorage.removeItem("token");

    // Redirect
    window.location.replace(redirectURL);
  }

  private token: string;

  getToken() {
    this.token = JSON.parse(localStorage.getItem("token"));

    if (!this.token && location.hash) {
      const params = new HttpParams({
        fromString: location.hash.substr(1)
      });
      this.token = params.get("access_token");

      localStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}

export interface SecurityConfig {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}
