import { TestBed, inject } from '@angular/core/testing';

import { SecurityService, SECURITY_CONFIG } from './security.service';

describe('SecurityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: SECURITY_CONFIG, useValue:{}
        },
        SecurityService
      ]
    });
  });

  it('should be created', inject([SecurityService], (service: SecurityService) => {
    expect(service).toBeTruthy();
  }));
});
