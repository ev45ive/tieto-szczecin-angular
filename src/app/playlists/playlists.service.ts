import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Playlist } from "src/app/models/playlist";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  playlists: Playlist[] = [

  ];

  getPlaylists(): Observable<Playlist[]> {
    return of(this.playlists);
  }

  getPlaylist(id: number): Observable<Playlist> {
    return of(this.playlists.find(p => p.id == id));
  }

  savePlaylist(playlist: Playlist): Observable<Playlist> {
    const index = this.playlists.findIndex(old => old.id == playlist.id);
    this.playlists.splice(index, 1, playlist);
    return this.getPlaylist(playlist.id);
  }

  constructor() {}
}
