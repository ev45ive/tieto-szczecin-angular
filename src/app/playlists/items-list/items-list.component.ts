import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from "../../models/playlist";
import {
  NgForOfContext,
  NgForOf
} from "../../../../node_modules/@angular/common";
NgForOfContext;
NgForOf;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"]
  // encapsulation: ViewEncapsulation.None,
  // inputs:[
  //   'playlists:inputs'
  // ]
})
export class ItemsListComponent implements OnInit {

  @Input()
  selected: Playlist | null;

  @Input()
  items: Playlist[];

  @Output() 
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  trackFn(index, elem) {
    // console.log(index,elem)
    return elem.id;
  }

  constructor() {
    /*    setInterval(()=>{
      this.playlists = [...this.playlists]
      this.playlists.push(this.playlists.shift())
    },1500) */
  }

  ngOnInit() {}
}
