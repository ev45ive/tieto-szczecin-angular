import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlaylistDetailsComponent } from "./playlist-details.component";
import { FormsModule } from "../../../../node_modules/@angular/forms";
import { Component } from "../../../../node_modules/@angular/core";
import { Playlist } from "src/app/models/playlist";
import { By } from "../../../../node_modules/@angular/platform-browser";
import { DebugElement } from "@angular/core";

@Component({
  template: `<app-playlist-details [playlist]="playlist" ></app-playlist-details>`
})
class PlaylistDetailsMockComponent {}

describe("PlaylistDetailsComponent", () => {
  let component: PlaylistDetailsMockComponent;
  let fixture: ComponentFixture<PlaylistDetailsMockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [PlaylistDetailsMockComponent, PlaylistDetailsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsMockComponent);
    fixture.debugElement.context.playlist = {
      id: 123,
      name: "Test",
      favourite: false,
      color: "#ff0000"
    };
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should emit save event", async(() => {
    const details = fixture.debugElement.query(
      By.directive(PlaylistDetailsComponent)
    );
    const detailsInstance = details.componentInstance as PlaylistDetailsComponent;

    detailsInstance.playlistSave.subscribe(playlist => {
      expect(playlist.id).toEqual(123);
    });
  }));
});
