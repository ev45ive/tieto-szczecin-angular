import { Component, OnInit, Input } from "@angular/core";
import { Playlist } from "src/app/models/playlist";
import { ActivatedRoute } from "../../../../node_modules/@angular/router";
import { Router } from "@angular/router";
import { map, switchMap } from "rxjs/operators";
import { Observable } from "rxjs";
import { PlaylistsService } from "../playlists.service";

@Component({
  selector: "app-playlists",
  templateUrl: "./playlists.component.html",
  styleUrls: ["./playlists.component.scss"]
})
export class PlaylistsComponent implements OnInit {
  savePlaylist(playlist) {
    this.playlistsService.savePlaylist(playlist);
  }

  selected$ = this.route.paramMap.pipe(
    map(paramMap => parseInt(paramMap.get("id"), 10)),
    switchMap(id => this.playlistsService.getPlaylist(id))
  );

  playlists$ = this.playlistsService.getPlaylists();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService
  ) {}

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id]);
  }

  ngOnInit() {}
}
