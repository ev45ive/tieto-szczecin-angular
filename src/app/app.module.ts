import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { MusicModule } from "./music/music.module";
import { SecurityModule } from "./security/security.module";
import { SECURITY_CONFIG, SecurityService } from "./security/security.service";
import { environment } from "src/environments/environment";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    PlaylistsModule,
    MusicModule,
    AppRoutingModule,
    SecurityModule
  ],
  providers: [
    {
      provide: SECURITY_CONFIG,
      useValue: environment.security
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  
  constructor(private security: SecurityService) {
    security.getToken()
  }
}
