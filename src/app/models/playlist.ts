export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
   * List of tracks
   */
  tracks?: Track[]
}

// track.interface.ts
export interface Track {}
