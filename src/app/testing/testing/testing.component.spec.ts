import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";

import { TestingComponent, AbstractMessageService } from "./testing.component";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

describe("TestingComponent", () => {
  let fixture: ComponentFixture<TestingComponent>;
  let component: TestingComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      providers: [AbstractMessageService],
      declarations: [TestingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render initial text", () => {
    const p = fixture.debugElement.query(By.css(".message"));
    expect(p.nativeElement.innerText).toEqual("Hello Testing!");
  });

  it("should render changed text", () => {
    component.message = "changed text";

    fixture.detectChanges();

    const p = fixture.debugElement.query(By.css(".message"));
    expect(p.nativeElement.innerText).toEqual("changed text");
  });

  it("should take user text input", () => {
    const input = fixture.debugElement.query(By.css("input"));

    input.nativeElement.value = "text from user";

    // Emit simulated event object:
    input.triggerEventHandler("input", {
      target: input.nativeElement
    });

    // OR:

    // Emit Native Event:
    input.nativeElement.dispatchEvent(new Event("input"));

    expect(component.message).toEqual("text from user");
  });

  it("should render initial message inside input", async(() => {
    const input = fixture.debugElement.query(By.css("input"));

    fixture.whenStable().then(() => {
      expect(input.nativeElement.value).toEqual("Hello Testing!");
    });
  }));

  it("should update input on message change", async(() => {
    const input = fixture.debugElement.query(By.css("input"));

    component.message = "changed text";
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(input.nativeElement.value).toEqual("changed text");
    });
  }));

  it("should get message from service", inject(
    [AbstractMessageService],
    service => {
      // jasmine.createSpyObj('Service',['getMessage'])
      // jasmine.createSpy('getMessage')
      const spy = spyOn(service, "getMessage").and.returnValue(
        "message from service"
      );
      component.fetchMessage()
      
      expect(spy).toHaveBeenCalled();

      expect(component.message).toEqual("message from service");
    }
  ));
});
