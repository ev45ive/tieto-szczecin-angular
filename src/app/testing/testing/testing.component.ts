import { Component, OnInit } from "@angular/core";

export abstract class AbstractMessageService {
  getMessage() {
    return "";
  }
}

@Component({
  selector: "app-testing",
  templateUrl: "./testing.component.html",
  styleUrls: ["./testing.component.scss"]
})
export class TestingComponent implements OnInit {
  message: string = "Hello Testing!";

  constructor(private service: AbstractMessageService) {}

  fetchMessage() {
    this.message = this.service.getMessage();
  }

  ngOnInit() {}
}
